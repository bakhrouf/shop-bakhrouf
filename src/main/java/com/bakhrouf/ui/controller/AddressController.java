package com.bakhrouf.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bakhrouf.bll.entities.Address;
import com.bakhrouf.dal.dao.AddressRepository;
/*
 * @RestController public class AddressController {
 * 
 * @Autowired AddressRepository addressRepostory;
 * 
 * @GetMapping("/getaddress") List<Address> getAddress() {
 * System.out.println("get works"); return addressRepostory.findAll(); } String
 * getTestAddress() { return "hello"; }
 * 
 * 
 * }
 */
