package com.bakhrouf.bll.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Product {
	@Id@GeneratedValue(strategy=GenerationType.IDENTITY)

	private long id;
	private String title;
	private String description;
	private double price;
	private Status status;
	@OneToOne
	@JoinColumn(name="IdCategory")
	private Category category;
	@OneToOne
	@JoinColumn(name="IdProductDetails")
	private ProductDetails productDetails;
	

}
