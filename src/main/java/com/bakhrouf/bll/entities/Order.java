package com.bakhrouf.bll.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Order {
	@Id@GeneratedValue(strategy=GenerationType.IDENTITY)

	private Long id;
	private String reference;
	@OneToOne
	@JoinColumn(name="IdDelivery")
	private Delivery delivery;
	@OneToOne
	@JoinColumn(name="IdAddress")
	private Address address;
	@OneToOne
	@JoinColumn(name="IdCustomer")
	private Customer customer;
	@ManyToOne
	@JoinColumn(name="IdOrderProducts")
	private OrderProduct orderProduct;

}
