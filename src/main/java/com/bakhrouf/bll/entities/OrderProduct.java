package com.bakhrouf.bll.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class OrderProduct {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
private Long id;
	@OneToOne
	@JoinColumn(name="IdProduct")
private Product product;
private int quantity;
}
