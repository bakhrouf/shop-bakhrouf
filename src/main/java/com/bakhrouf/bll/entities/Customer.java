package com.bakhrouf.bll.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity

@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Customer {
	@Id@GeneratedValue(strategy=GenerationType.IDENTITY)
	

	private Long id;
	private String name;
	private String surname;
	private String login;
	private String password;
	private String email;
	@OneToOne
	@JoinColumn(name="IdAddress")
	private Address address;

}
