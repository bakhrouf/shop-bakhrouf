package com.bakhrouf.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import com.bakhrouf.bll.entities.Address;
import com.bakhrouf.dal.dao.AddressRepository;



@SpringBootApplication
@EntityScan("com.bakhrouf.bll.entities")
@EnableJpaRepositories("com.bakhrouf.dal.dao")
@ComponentScan("com.bakhrouf.ui.controller")
public class ShopApplication implements CommandLineRunner{
	@Autowired
	private AddressRepository addressRepository;

	public static void main(String[] args) {
		SpringApplication.run(ShopApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub


	}

}
