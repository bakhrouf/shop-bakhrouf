package com.bakhrouf.dal.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bakhrouf.bll.entities.Product;

public interface ProductRepository6 extends JpaRepository<Product, Long>{

}
