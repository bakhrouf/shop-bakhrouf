package com.bakhrouf.dal.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bakhrouf.bll.entities.Delivery;
import com.bakhrouf.bll.entities.Product;

public interface DeliveryRepository extends JpaRepository<Delivery, Long>{

}
