package com.bakhrouf.dal.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bakhrouf.bll.entities.Customer;
import com.bakhrouf.bll.entities.Product;

public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
