package com.bakhrouf.dal.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.bakhrouf.bll.entities.Address;
@RepositoryRestResource
public interface AddressRepository extends JpaRepository<Address, Long>{
	@RestResource(path="/byCity")
	public List<Address> findByCity(@Param("mc") String city);
	@RestResource(path="/byCityPage")
	public Page<Address> findByCityContains(@Param("mc") String city, Pageable pageable);
}
