package com.bakhrouf.dal.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bakhrouf.bll.entities.Product;
import com.bakhrouf.bll.entities.Status;

public interface StatusRepository extends JpaRepository<Status, Long>{

}
