package com.bakhrouf.dal.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bakhrouf.bll.entities.Order;
import com.bakhrouf.bll.entities.Product;

public interface OrderRepository extends JpaRepository<Order, Long>{

}
